import axios, { AxiosInstance } from 'axios';
import { SomeResponseData } from '../types/responses';

const { apiBaseUrl } = {
  apiBaseUrl: "apiUrl"
};

export const createAxiosInstance = async (): Promise<AxiosInstance> => {
  const instance = axios.create({
    baseURL: apiBaseUrl,
    withCredentials: true,
  });

   return instance;
};

let axiosInstance: AxiosInstance;

async function getInstace(): Promise<AxiosInstance> {
  if (axiosInstance) {
    return axiosInstance;
  }

  axiosInstance = await createAxiosInstance();
  return axiosInstance;
}

const Api = {
  dataItem: {
    async getItems(options = {}) {
      const instance = await getInstace();
      return instance.post<SomeResponseData>('api/dataItem/getItems', options);
    },
  },
  other: {
    // eslint-disable-next-line id-denylist
    async otherMethod(data: any, params?: any) {
      const instance = await getInstace();
      return instance.post('api/other/otherMethod', data, {
        params,
      });
    },
  },
};

export default Api;
