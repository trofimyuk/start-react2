import React from 'react';
import { StartStore } from './StartStore';

class RootStore {
  startStore: StartStore;

  constructor() {
    this.startStore = new StartStore();
  }
}

const rootStoresContext = React.createContext(new RootStore());

// this will be the function available for the app to connect to the stores
export const useRootStores = () => React.useContext(rootStoresContext);

const rootStore = new RootStore();
export default rootStore;
