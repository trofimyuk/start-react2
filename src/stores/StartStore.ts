import { makeAutoObservable } from 'mobx';
import { SomeResponseData } from '../types/responses';
import Api from '../utils/api';


export class StartStore {
  // TODO: See if we can narrow this down
  startStore: Record<string, any> = {};

  constructor() {
    makeAutoObservable(this);
  }

  async getItems(options: any): Promise<SomeResponseData> {
    if (this.startStore['items']) return this.startStore['items'];
    this.startStore['items'] = await Api.dataItem.getItems(options);
    return this.startStore['items'];
  }
}
